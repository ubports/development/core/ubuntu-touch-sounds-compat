# Ubuntu Touch Sounds Compatibility

Provides three packages that allow us to have Lomiri in other distributions while maintaining a clean upgrade path for Ubuntu Touch users.

## old-ubuntu-sounds-compat

Creates a link from `/usr/share/sounds/ubuntu/` to `/usr/share/sounds/lomiri`. People who have had their Ubuntu Touch devices for a very long time need this link. Ubuntu Touch software stores a path to the file in user storage rather than copying the user's desired notification sounds to their home folder. Therefore, if someone set `/usr/share/sounds/ubuntu/Ubuntu.ogg` as their notification sound, that file must be reachable. If the file is unreachable, it simply will not play.

Broken links are bad, so this depends on `lomiri-sounds`.

## ubports-sounds-compat

ubports-sounds-compat has similar reasoning and requirements as old-ubuntu-sounds-compat, but this time it links `/usr/share/sounds/ubports/` to `/usr/share/sounds/lomiri.`.

## ubuntu-touch-sounds

ubuntu-touch-sounds is a transitional package that gives users the same experience as the old `ubuntu-touch-sounds` binary package by depending upon `old-ubuntu-sounds-compat` and `ubports-sounds-compat`.
